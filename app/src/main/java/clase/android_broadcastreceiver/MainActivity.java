package clase.android_broadcastreceiver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer {

    private Button buttonMyNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ObservableObject.getInstance().addObserver(this);

        buttonMyNotification  = (Button) findViewById(R.id.buttonMyNotification);
        buttonMyNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction("clase.android_boradcastreceiver.MY_NOTIFICATION");
                //intent.putExtra("data","Notice me senpai!");
                sendBroadcast(intent);
                //Se utiliza cuando queremos mandar un broadcast pero con privilegios asociados
                //sendOrderedBroadcast(Intent, String);
                //https://developer.android.com/guide/components/broadcasts.html#sending_broadcasts
            }
        });

    }

    @Override
    public void update(Observable observable, Object data) {

        Intent i = (Intent) data;

        if(i.getAction().contains("AIRPLANE_MODE")){
            TextView tx = (TextView) findViewById(R.id.texto);
            tx.setText(String.valueOf(i.getBooleanExtra("broadcastreceiverModePlane",false)));
        }



        //Futuras acciones sobre otro broadcast
        /*if(i.getAction().equals("android.intent.action.BateryLow")){
            //...
        }*/



    }
}
