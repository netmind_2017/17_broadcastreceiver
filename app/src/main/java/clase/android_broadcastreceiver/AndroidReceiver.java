package clase.android_broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.widget.Toast;

public class AndroidReceiver extends BroadcastReceiver {
    public AndroidReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if(Settings.System.getInt(context.getContentResolver(),Settings.System.AIRPLANE_MODE_ON, 0) != 0){
            //Toast.makeText(context, "avion off", Toast.LENGTH_LONG).show();
            ObservableObject.getInstance().updateValue(intent.putExtra("broadcastreceiverModePlane",false));

        }else{
            //Toast.makeText(context, "avion on", Toast.LENGTH_LONG).show();
            ObservableObject.getInstance().updateValue(intent.putExtra("broadcastreceiverModePlane",true));
        }
    }
}
