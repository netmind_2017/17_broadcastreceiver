package clase.android_broadcastreceiver;


import java.util.Observable;

public class ObservableObject extends Observable {
    private static ObservableObject instance = new ObservableObject();

    public static ObservableObject getInstance() {
        return instance;
    }

    private ObservableObject() {
    }

     /*
      * Metodo por el cual para cada activity que este solicitando un evento update
      * El metodo notifica a todas las activitys implicadas
      * */
    public void updateValue(Object data) {
        synchronized (this) {
            setChanged();
            notifyObservers(data);
        }
    }
}
